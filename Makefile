all:
	javac -d bin -cp :* src/*.java
	jar cvfm raytrace.jar manifest.txt -C bin .
	jarsigner -keystore omamstore raytrace.jar omam