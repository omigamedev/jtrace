
class Bezier2D
{
	public static final int DIR_U = 0;
	public static final int DIR_V = 1;

	public Vec2[][] cp = new Vec2[4][4];
	public Vec2 rangea = new Vec2(1, 1);
	public Vec2 rangeb = new Vec2(0, 0);

	public Bezier2D()
	{
	}

	public Bezier2D(Vec2[][] ccp)
	{
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				this.cp[i][j] = new Vec2(ccp[i][j].x, ccp[i][j].y);
			}
		}
	}

	public Bezier2D(Ray r, Vec4[][] cp3)
	{
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				cp[i][j] = r.project(cp3[i][j]);
			}
		}
	}
	
	public Vec2 getCenterUV()
	{
		return new Vec2(rangea.x + (rangeb.x - rangea.x) * 0.5f, 
				rangea.y + (rangeb.y - rangea.y) * 0.5f);
	}

	public Vec2 deCasteljau(Vec2[] cp, float t, Vec2[] r, Vec2[] l)
	{
		Vec2[] p = cp.clone();
		for (int i = 0; i < 4; i++)
		{
			p[i] = cp[i].clone();
		}
		for (int i = 4; i > 0; i--)
		{
			if (l != null) l[4 - i] = p[0].clone();
			if (r != null) r[i - 1] = p[i - 1].clone();
			for (int j = 0; j < i - 1; j++)
			{
				p[j].x = p[j].x * (1.0f - t) + p[j + 1].x * t;
				p[j].y = p[j].y * (1.0f - t) + p[j + 1].y * t;
			}
		}
		return p[0];
	}

	public Bezier2D[] subdivide(int dir)
	{
		Vec2[] curve = new Vec2[4];
		Vec2[][] parts = new Vec2[2][4];
		Vec2[][] r = new Vec2[4][4];
		Vec2[][] l = new Vec2[4][4];
		for (int i = 0; i < 4; i++)
		{
			if (dir == DIR_U)
			{
				deCasteljau(cp[i].clone(), 0.5f, l[i], r[i]);
			}
			else
			{
				for (int n = 0; n < 4; n++)
					curve[n] = cp[n][i];
				deCasteljau(curve, 0.5f, parts[0], parts[1]);
				for (int n = 0; n < 4; n++)
				{
					l[n][i] = parts[0][n];
					r[n][i] = parts[1][n];
				}
			}
		}
		Bezier2D br = new Bezier2D(r);
		Bezier2D bl = new Bezier2D(l);
		Vec2 centerUV = getCenterUV();
		if(dir == DIR_U) // x
		{
			bl.rangea.x = rangea.x;
			bl.rangeb.x = centerUV.x;
			bl.rangea.y = rangea.y;
			bl.rangeb.y = rangeb.y;
			
			br.rangea.x = centerUV.x;
			br.rangeb.x = rangeb.x;
			br.rangea.y = rangea.y;
			br.rangeb.y = rangeb.y;
		}
		else // y
		{
			bl.rangea.y = rangea.y;
			bl.rangeb.y = centerUV.y;
			bl.rangea.x = rangea.x;
			bl.rangeb.x = rangeb.x;
			
			br.rangea.y = centerUV.y;
			br.rangeb.y = rangeb.y;
			br.rangea.x = rangea.x;
			br.rangeb.x = rangeb.x;
		}
		return new Bezier2D[] { bl, br };
	}

	public boolean inside(Vec2 p)
	{
		boolean h = true, v = true;
		for (int i = 0; i < 4 && (h | v); i++)
		{
			for(int j=0; j<4;j++)
			{
				if (cp[i][j].x * cp[0][0].x <= 0.0f) h = false;
				if (cp[i][j].y * cp[0][0].y <= 0.0f) v = false;
			}
		}
		return !(h | v);
	}
}