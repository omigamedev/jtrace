import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Options extends JFrame
{
	public static JSlider sliderIteration;
	public static JCheckBox chkControlPolygon;
	public static JCheckBox chkControlPoints;
	public static JCheckBox chkRaytrace;
	public static JCheckBox chkDiffuse;
	public static JCheckBox chkSupersampling;
	public static JCheckBox chkWireframe;
	public static JCheckBox chkNormals;
	public static JButton btnResetBezier;
	public static JButton btnRandomizeBezier;
	public static JButton btnResetView;
	public static JCheckBox chkTexture;
	public static JSlider sliderFitting;
	public static JSlider sliderFov;
	public static JSlider sliderPhongAlpha;
	public static JSlider sliderPhongI;
	public static JSlider sliderInteractivity;
	public static JSlider sliderSupersampling;
	private JButton btnSwitch;

	public Options()
	{
		setSize(200, 400);
		setLocation(Main.w*Main.downsample+Main.view.getX(), 0);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Options");
		setResizable(false);
		
		setLayout(new GridLayout(0, 2));
		
		btnResetBezier = new JButton("Reset Control Points");
		btnResetBezier.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Main.new_reset = true;
			}
		});
		add(btnResetBezier);
		
		btnRandomizeBezier = new JButton("Randomize Control Points");
		btnRandomizeBezier.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Main.new_randomize = true;
			}
		});
		add(btnRandomizeBezier);
		
		btnResetView = new JButton("Reset View");
		btnResetView.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Main.new_view = true;
			}
		});
		add(btnResetView);
		
		btnSwitch = new JButton("Switch to Wireframe");
		btnSwitch.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if(Options.chkRaytrace.isSelected())
				{
					Options.chkRaytrace.setSelected(false);
					Options.chkControlPoints.setSelected(true);
					Options.chkControlPolygon.setSelected(true);
					Options.chkWireframe.setSelected(true);
					btnSwitch.setText("Switch to Raytrace");
				}
				else
				{
					Options.chkRaytrace.setSelected(true);
					Options.chkControlPoints.setSelected(false);
					Options.chkControlPolygon.setSelected(false);
					Options.chkWireframe.setSelected(false);
					btnSwitch.setText("Switch to Wireframe");
				}
			}
		});
		add(btnSwitch);
		
		chkControlPolygon = new JCheckBox("Control Polyhedra", false);
		add(chkControlPolygon);
		
		chkRaytrace = new JCheckBox("Raytrace", true);
		chkRaytrace.addItemListener(new ItemListener()
		{
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				switch(e.getStateChange())
				{
				case ItemEvent.DESELECTED:
					Main.g0.clearRect(0, 0, Main.view.img.getWidth(), Main.view.img.getHeight());
					break;
				}
			}
		});
		add(chkRaytrace);
		
		chkControlPoints = new JCheckBox("Control Points", false);
		add(chkControlPoints);
		
		chkTexture = new JCheckBox("Textured", true);
		add(chkTexture);
		
		chkDiffuse = new JCheckBox("Phong Shading", true);
		add(chkDiffuse);
		
		chkSupersampling = new JCheckBox("Super Sampling", false);
		chkSupersampling.addItemListener(new ItemListener()
		{
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				switch(e.getStateChange())
				{
				case ItemEvent.DESELECTED:
					Main.new_samples = 1;
					System.out.println(e);
					break;
				case ItemEvent.SELECTED:
					Main.new_samples = 5;
					System.out.println(e);
					break;
				}
			}
		});
		add(chkSupersampling);

		chkWireframe = new JCheckBox("Wireframe View", false);
		add(chkWireframe);
		
		chkNormals = new JCheckBox("Surface Normals", false);
		add(chkNormals);
		
		sliderFitting = new JSlider(1, 100, 20);
		final JLabel sliderFittingLabel = new JLabel("  Wireframe Subdivisions: " + sliderFitting.getValue());
		sliderFitting.addChangeListener(new ChangeListener()
		{
			@Override
			public void stateChanged(ChangeEvent e)
			{
				sliderFittingLabel.setText("  Wireframe Subdivisions: " + sliderFitting.getValue());
			}
		});
		add(sliderFittingLabel);
		add(sliderFitting);

		sliderInteractivity = new JSlider(1, 30, 4);
		final JLabel sliderInteractivityLabel = new JLabel("  Interactivity: " +
				sliderInteractivity.getValue());
		sliderInteractivity.addChangeListener(new ChangeListener()
		{
			@Override
			public void stateChanged(ChangeEvent e)
			{
				sliderInteractivityLabel.setText("  Interactivity: " + sliderInteractivity.getValue());
				Main.render_div = sliderInteractivity.getValue();
			}
		});
		Main.render_div = sliderInteractivity.getValue();
		add(sliderInteractivityLabel);
		add(sliderInteractivity);
		
		sliderIteration = new JSlider(1, 30, 20);
		final JLabel sliderIterationLabel = new JLabel("  Iterations: " + sliderIteration.getValue());
		sliderIteration.addChangeListener(new ChangeListener()
		{
			@Override
			public void stateChanged(ChangeEvent e)
			{
				sliderIterationLabel.setText("  Iterations: " + sliderIteration.getValue());
			}
		});
		add(sliderIterationLabel);
		add(sliderIteration);
		

		sliderFov = new JSlider(3, 30, 5);
		final JLabel sliderFovLabel = new JLabel("  Perspective: " + sliderFov.getValue());
		sliderFov.addChangeListener(new ChangeListener()
		{
			@Override
			public void stateChanged(ChangeEvent e)
			{
				sliderFovLabel.setText("  Perspective: " + sliderFov.getValue());
				Main.new_po.z = -sliderFov.getValue() * 0.1f;
			}
		});
		Main.new_po.z = -sliderFov.getValue() * 0.1f;
		add(sliderFovLabel);
		add(sliderFov);
		
		sliderPhongAlpha = new JSlider(1, 30, 20);
		final JLabel sliderPhongAlphaLabel = new JLabel("  Specular Power: " + sliderPhongAlpha.getValue());
		sliderPhongAlpha.addChangeListener(new ChangeListener()
		{
			@Override
			public void stateChanged(ChangeEvent e)
			{
				sliderPhongAlphaLabel.setText("  Specular Power: " + sliderPhongAlpha.getValue());
			}
		});
		add(sliderPhongAlphaLabel);
		add(sliderPhongAlpha);
		
		
		sliderPhongI = new JSlider(1, 100, 100);
		final JLabel sliderPhongILabel = new JLabel("  Specular Intensity: " + sliderPhongI.getValue());
		sliderPhongI.addChangeListener(new ChangeListener()
		{
			@Override
			public void stateChanged(ChangeEvent e)
			{
				sliderPhongILabel.setText("  Specular Intensity: " + sliderPhongI.getValue());
				Main.bp.specular = sliderPhongI.getValue() * 0.01f;
			}
		});
		add(sliderPhongILabel);
		add(sliderPhongI);
		
		pack();
	}
}
