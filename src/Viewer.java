import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Viewer extends JFrame implements MouseMotionListener, MouseListener, MouseWheelListener
{
	public BufferedImage img, img2;
	public float mouseX = 0.0f;
	public float mouseY = 0.0f;

	public Sphere dragObject, hoverObject;
	public Vec2 dragStart;
	public Plane dragPlane;
	public int dragI, dragJ;
	
	public Viewer(int w, int h)
	{
		img = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		img2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		setSize(w*Main.downsample, h*Main.downsample);
		setVisible(true);
		setResizable(false);
		createBufferStrategy(2);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().addMouseMotionListener(this);
		getContentPane().addMouseListener(this);
		getContentPane().addMouseWheelListener(this);
		
		Graphics g = getGraphics();
		g.clearRect(0, 0, getWidth(), getHeight());
		Font f = new Font("TimesRoman", Font.BOLD, 33);
		g.setFont(f);
		String s = "Loading...";
		g.drawString(s, (getWidth()-getFontMetrics(f).stringWidth(s))/2, getHeight()/2);
		//g.finalize();
		//repaint();
	}

	public void swap()
	{
		BufferStrategy bf = this.getBufferStrategy();
		Graphics2D g = (Graphics2D)bf.getDrawGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		//if(Options.chkRaytrace.isSelected())
			g.drawImage(img, 0, 0, Main.w*Main.downsample, Main.h*Main.downsample, null);
		//if(Options.chkControlPoints.isSelected() || Options.chkControlPolygon.isSelected())
			g.drawImage(img2, 0, 0, Main.w*Main.downsample, Main.h*Main.downsample, null);
		g.dispose();
		bf.show();
		Toolkit.getDefaultToolkit().sync();
	}

	@Override
	public void paintComponents(Graphics g)
	{
	}

	@Override
	public void paint(Graphics g)
	{
	}

	@Override
	public void paintAll(Graphics g)
	{
	}

	@Override
	public void mouseDragged(MouseEvent e)
	{
		Point m = SwingUtilities.convertPoint(getContentPane(), e.getPoint(), this);
		mouseX = (float)m.x / (float)getWidth() - 0.5f;
		mouseY = 1 - (float)(m.y) / (float)getHeight() - 0.5f;
		if (dragObject != null)
		{
			Ray ray = Main.createRay(mouseX, mouseY);
			float t = dragPlane.intersectUnbound(ray);
			Vec4 p = ray.o.add(ray.d.mul(t));
			Vec4 unprojected = Main.modelview_inverse.mul(p);
			dragObject.c_original.x = unprojected.x;
			dragObject.c_original.y = unprojected.y;
			dragObject.c_original.z = unprojected.z;
//			Main.bp.cp_original[dragI][dragJ].x = is.p.x;
//			Main.bp.cp_original[dragI][dragJ].y = is.p.y;
		}
		else
		{
			Vec2 diff = new Vec2(mouseX, mouseY).ssub(dragStart);
			Main.rot.x = Main.oldRot.x + diff.x;
			Main.rot.y = Main.oldRot.y + diff.y;
			//Main.g.clearRect(0, 0, img2.getWidth(), img2.getHeight());
		}
	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
		Point m = SwingUtilities.convertPoint(getContentPane(), e.getPoint(), this);
		mouseX = (float)m.x / (float)getWidth() - 0.5f;
		mouseY = 1 - (float)(m.y) / (float)getHeight() - 0.5f;
		
		Ray ray = Main.createRay(mouseX, mouseY);
		Intersection winner = Intersection.find(ray, Main.objects, Main.bezlist[0]);
		if (winner != null && winner.obj != dragObject && winner.obj instanceof Sphere)
		{
			hoverObject = (Sphere)winner.obj;
		}
		else
		{
			hoverObject = null;
		}
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		Ray ray = Main.createRay(mouseX, mouseY);
		Intersection winner = Intersection.find(ray, Main.objects, Main.bezlist[0]);
		if (winner != null && winner.obj != dragObject && winner.obj instanceof Sphere)
		{
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					Sphere sp = (Sphere)Main.objects[i * 4 + j + 1];
					if(sp == winner.obj)
					{
						dragI = i;
						dragJ = j;
					}
				}
			}
			dragObject = (Sphere)winner.obj;
			dragPlane = new Plane(dragObject.c, new Vec4(0, 0, -1), null);
			Main.moving = dragObject.c;
		}
		else
		{
			dragObject = null;
			Main.oldRot.x = Main.rot.x;
			Main.oldRot.y = Main.rot.y;
		}
		dragStart = new Vec2(mouseX, mouseY);
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		if(dragObject == null)
		{
			Main.modelview_original.copy(Main.modelview);
		}
		dragObject = null;
		dragStart = null;
		Main.moving = null;
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e)
	{
		if (e.getWheelRotation() > 0)
		{
			Main.bezier_z += 0.1f;
		}
		else
		{
			Main.bezier_z -= 0.1f;
		}
		System.out.println("bezier_z " + Main.bezier_z);
	}
}
