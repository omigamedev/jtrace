import java.util.ArrayList;

public class Bezier3DPatch extends Object3D
{
	public Vec4[][] cp = new Vec4[4][4];
	public Vec4[][] cp_original = new Vec4[4][4];

	public Bezier3DPatch()
	{
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				cp_original[i][j] = new Vec4();
	}

	public void setControlPoints(Vec4[][] ccp)
	{
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
			{
				cp_original[i][j].x = ccp[i][j].x;
				cp_original[i][j].y = ccp[i][j].y;
			}
	}

	public Vec4 deCasteljau2D(Vec4[] pp, float t)
	{
		Vec4[] p = new Vec4[4];

		for (int i = 0; i < 4; i++)
			p[i] = pp[i].clone();

		for (int i = 4; i > 0; i--)
		{
			for (int j = 0; j < i - 1; j++)
			{
				p[j].x = p[j].x * (1.0f - t) + p[j + 1].x * t;
				p[j].y = p[j].y * (1.0f - t) + p[j + 1].y * t;
				p[j].z = p[j].z * (1.0f - t) + p[j + 1].z * t;
			}
		}
		return p[0];
	}

	public Vec4 deCasteljau3D(float u, float v)
	{
		Vec4[] cptmp = new Vec4[4];
		for (int i = 0; i < 4; i++)
		{
			cptmp[i] = deCasteljau2D(cp[i], u);
		}
		return deCasteljau2D(cptmp, v);
	}

	@Override
	public Intersection intersect(Ray ray)
	{
		int dir = 0;
		Vec2 zero = new Vec2();
		Bezier2D bz = new Bezier2D(ray, cp);
		if (!bz.inside(zero)) return null;
		// return new Intersection(5.0f, this, ray, true);
		// *
		ArrayList<Bezier2D> blist = new ArrayList<Bezier2D>();
		blist.add(bz);
		int niter = Options.sliderIteration.getValue();
		for (int n = 0; n < niter; n++)
		{
			ArrayList<Bezier2D> blisttmp = new ArrayList<Bezier2D>();
			for (Bezier2D b : blist)
			{
				Bezier2D[] rl = b.subdivide(dir);
				if (rl[0].inside(zero)) blisttmp.add(rl[0]);
				if (rl[1].inside(zero)) blisttmp.add(rl[1]);
			}
			dir = (dir + 1) % 2;
			blist = blisttmp;
			if (blist.size() == 0) return null;
		}
		float minz = Float.MAX_VALUE;
		Vec4 minp = null;
		Vec2 minuv = null;
		for (Bezier2D b : blist)
		{
			Vec2 uv = b.getCenterUV();
			Vec4 p = deCasteljau3D(uv.x, uv.y);
			if (p.z < minz)
			{
				minp = p;
				minz = p.z;
				minuv = uv;
			}
		}
		// BezierIntersection is = new BezierIntersection(10, this, ray, true,
		// 0, 0);
		// return is;
		// compute normal
		Vec4 uu = deCasteljau3D(minuv.x + 0.01f, minuv.y).sub(deCasteljau3D(minuv.x - 0.01f, minuv.y));
		Vec4 vv = deCasteljau3D(minuv.x, minuv.y + 0.01f).sub(deCasteljau3D(minuv.x, minuv.y - 0.01f));
		Vec4 n = vv.cross(uu).normalize();
		BezierIntersection is = new BezierIntersection(minp.sub(ray.o).len(), this, ray, true, minuv.x, minuv.y);
		is.p = minp;
		// is.n = new Vec4(minuv.x*2-1, minuv.y*2-1, 0);//n;
		is.n = n;

		return is;
		// */
	}
	
	@Override
	public Vec4 getColor(Intersection is)
	{
		BezierIntersection bis = (BezierIntersection)is;
		int modu = (int)Math.floor(bis.u * 10.0f) % 2;
		int modv = (int)Math.floor(bis.v * 10.0f) % 2;
		Vec4 col = modu == modv ? new Vec4(1,1,1) : new Vec4(0,0,1);
		return col;
	}

	@Override
	public void completeIntersection(Ray ray, Intersection is)
	{
	}

	@Override
	public void applyMat()
	{
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				cp[i][j] = modelview.mul(cp_original[i][j]);
			}
		}
	}
}
