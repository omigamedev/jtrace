public class Plane extends Object3D
{
	public Vec4 c, n;
	
	public Plane(Vec4 point, Vec4 normal, Vec4 color)
	{
		super();
		c = point;
		n = normal.normalize();
		this.color = color;
	}

	@Override
	public Intersection intersect(Ray ray)
	{
		float den = ray.d.dot(n);
		if (den == 0) return null;
		float num = c.sub(ray.o).dot(n);
		float t = num / den;
		return t < 0.0f ? null : new Intersection(t, this, ray, den < 0);
	}
	
	public float intersectUnbound(Ray ray)
	{
		float den = ray.d.dot(n);
		if (den == 0) return Float.NaN;
		float num = c.sub(ray.o).dot(n);
		float t = num / den;
		return t;
	}

	@Override
	public void completeIntersection(Ray ray, Intersection is)
	{
		is.p = ray.o.add(ray.d.mul(is.t));
		is.n = n;
	}

	@Override
	public void applyMat()
	{

	}
}
