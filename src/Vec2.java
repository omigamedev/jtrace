public class Vec2 implements Cloneable
{
	public float x, y;

	public Vec2()
	{
		x = y = 0.0f;
	}

	public Vec2(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public Vec2 clone()
	{
		return new Vec2(x, y);
	}
	
	public Vec2 clamp(float min, float max)
	{
		return new Vec2(_clamp(x, min, max), _clamp(y, min, max));
	}

	public Vec2 normalize()
	{
		return mul(1.0f / len());
	}
	
	public Vec2 neg()
	{
		return new Vec2(-x, -y);
	}

	public Vec2 add(Vec2 v)
	{
		return new Vec2(x + v.x, y + v.y);
	}
	
	public Vec2 add(float f)
	{
		return new Vec2(x + f, y + f);
	}

	public Vec2 sub(Vec2 v)
	{
		return new Vec2(x - v.x, y - v.y);
	}

	public Vec2 mul(float f)
	{
		return new Vec2(x * f, y * f);
	}

	public float dot(Vec2 v)
	{
		return x * v.x + y * v.y;
	}

	public float len()
	{
		return (float) Math.sqrt(x * x + y * y);
	}
	
	// SELF OPERATIONS
	
	private float _clamp(float f, float min, float max)
	{
		if(f < min) return min;
		if(f > max) return max;
		return f;
	}
	
	public Vec2 sclamp(float min, float max)
	{
		x = _clamp(x, min, max);
		y = _clamp(y, min, max);
		return this;
	}

	public Vec2 snormalize()
	{
		smul(1.0f / len());
		return this;
	}
	
	public Vec2 sneg()
	{
		x = -x;
		y = -y;
		return this;
	}

	public Vec2 sadd(Vec2 v)
	{
		x += v.x;
		y += v.y;
		return this;
	}
	
	public Vec2 sadd(float f)
	{
		x += f;
		y += f;
		return this;
	}

	public Vec2 ssub(Vec2 v)
	{
		x -= v.x;
		y -= v.y;
		return this;
	}

	public Vec2 smul(float f)
	{
		x *= f;
		y *= f;
		return this;
	}
	
	@Override
	public String toString()
	{
		return String.format("[%.2f %.2f]\n", x, y);
	}
}
