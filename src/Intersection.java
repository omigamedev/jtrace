public class Intersection
{
	public float t;
	public Object3D obj;
	public Ray r;
	public Vec4 p, n;
	public boolean front;

	public Intersection()
	{
	}

	public Intersection(float ft, Object3D object, Ray ray, boolean front)
	{
		t = ft;
		obj = object;
		r = ray;
	}

	public static Intersection find(Ray ray, Object3D[] objects, Object3D exclude)
	{
		Intersection winner = null;
		for (Object3D obj : objects)
		{
//			if (obj == null || exclude == obj || (exclude != null && obj == objects[0])) continue;
			if (obj == null || exclude == obj) continue;
			Intersection is = obj.intersect(ray);
			if (winner == null || is != null && is.t < winner.t)
			{
				winner = is;
			}
		}
		return winner;
	}

	public void complete()
	{
		obj.completeIntersection(r, this);
	}
}
