public class Sphere extends Object3D
{
	public Vec4 c_original, c;
	public float r;

	public Sphere()
	{
		r = 1.0f;
	}

	public Sphere(Vec4 center, float radius, Vec4 color)
	{
		c_original = center;
		r = radius;
		this.color = color;
	}
	
	class SphereIntersection extends Intersection
	{
		public Vec4 c;
		
		public SphereIntersection(float ft, Object3D object, Ray ray, Vec4 center)
		{
			super(ft, object, ray, true);
			c = center;
		}
	}

	@Override
	public Intersection intersect(Ray ray)
	{
		Vec4 oc = ray.o.sub(c); // oc = Q - C
		
		//float d = ray.d.cross(oc).len();
		//if(d > r) return null;
		
		float fa = ray.d.dot(ray.d); // fa = V²
		float fb = 2.0f * ray.d.dot(oc); // fb = 2 * V * oc
		float fc = oc.dot(oc) - r * r;
		float discr = fb * fb - 4 * fa * fc;
		if (discr < 0)
		{
			// no intersection
			return null;
		}
		else
		{
			float root1 = (float) ((-fb - Math.sqrt(discr)) / (2.0f * fa));
			float root2 = (float) ((-fb + Math.sqrt(discr)) / (2.0f * fa));
			float root = Math.min(root1, root2);
			if(root1 < 0.0f) root = root2;
			if(root2 < 0.0f) root = root1;
			if(root < 0.0f) return null;
			return new SphereIntersection(root, this, ray, c);
		}
	}

	@Override
	public void completeIntersection(Ray ray, Intersection intersection)
	{
		SphereIntersection is = (SphereIntersection)intersection;
		is.p = ray.o.add(ray.d.mul(is.t));
		is.n = is.p.sub(is.c).normalize();
	}

	@Override
	public void applyMat()
	{
		c = modelview.mul(c_original);
	}
}
