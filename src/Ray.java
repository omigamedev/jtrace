public class Ray
{
	public static int count = 0;
	
	public Vec4 o, d;
	public Vec4 frag;
	public Vec4 u, v;
	public Matrix4 m;
	public float light;
	public Intersection is;
	public int id;

	public Ray(Vec4 origin, Vec4 direction)
	{
		id = count++;
		o = origin;
		d = direction.normalize();
		
		// compute the frame
		Vec4 side = direction.cross(new Vec4(0, 1, 0)).normalize();
		Vec4 up = side.cross(direction).normalize();
//		Matrix4 rot = new Matrix4();
//		rot.m00 = side.x;
//		rot.m01 = side.y;
//		rot.m02 = side.z;
//		rot.m10 = up.x;
//		rot.m11 = up.y;
//		rot.m12 = up.z;
//		rot.m20 = direction.x;
//		rot.m21 = direction.y;
//		rot.m22 = direction.z;
//		rot.m33 = 1;
//		Matrix4 tr = new Matrix4();
//		tr.m03 = -origin.x;
//		tr.m13 = -origin.y;
//		tr.m23 = -origin.z;
//		tr.m33 = 1;
//		m = rot.mul(tr);
		u = side;
		v = up;
//		u.snormalize();
//		v.snormalize();
	}
	
	public float dist(Vec4 v)
	{
		return d.cross(v.sub(o)).len();// D x (V - O)
	}
	
	public Vec2 project(Vec4 vec)
	{
		//return m.mul(vec).toVec2();
		return new Vec2(u.dot(vec.sub(o)), v.dot(vec.sub(o)));
	}
}
