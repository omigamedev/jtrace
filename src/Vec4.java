public class Vec4 implements Cloneable
{
	public float x, y, z, w;

	public Vec4()
	{
		x = y = z = 0.0f;
		w = 1.0f;
	}

	public Vec4(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = 1.0f;
	}

	public Vec4(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}
	
	public Vec4 clone()
	{
		return new Vec4(x, y, z, w);
	}
	
	public Vec2 toVec2()
	{
		return new Vec2(x, y);
	}
	
	public Vec4 reflection(Vec4 n)
	{
		return n.mul(2*n.dot(this)).sub(this);
	}
	
	public Vec4 clamp(float min, float max)
	{
		return new Vec4(_clamp(x, min, max), _clamp(y, min, max), _clamp(z, min, max));
	}

	public Vec4 normalize()
	{
		return mul(1.0f / len());
	}
	
	public Vec4 neg()
	{
		return new Vec4(-x, -y, -z);
	}

	public Vec4 add(Vec4 v)
	{
		return new Vec4(x + v.x, y + v.y, z + v.z);
	}
	
	public Vec4 add(float f)
	{
		return new Vec4(x + f, y + f, z + f);
	}

	public Vec4 sub(Vec4 v)
	{
		return new Vec4(x - v.x, y - v.y, z - v.z);
	}

	public Vec4 mul(float f)
	{
		return new Vec4(x * f, y * f, z * f);
	}

	public Vec4 cross(Vec4 v)
	{
		return new Vec4(y * v.z - z * v.y, z * v.x - x * v.z,
				x * v.y - y * v.x);
	}

	public float dot(Vec4 v)
	{
		return x * v.x + y * v.y + z * v.z;
	}

	public float len()
	{
		return (float) Math.sqrt(x * x + y * y + z * z);
	}
	
	// SELF OPERATIONS
	
	private float _clamp(float f, float min, float max)
	{
		if(f < min) return min;
		if(f > max) return max;
		return f;
	}
	
	public Vec4 sclamp(float min, float max)
	{
		x = _clamp(x, min, max);
		y = _clamp(y, min, max);
		z = _clamp(z, min, max);
		return this;
	}

	public Vec4 snormalize()
	{
		smul(1.0f / len());
		return this;
	}
	
	public Vec4 sneg()
	{
		x = -x;
		y = -y;
		z = -z;
		return this;
	}

	public Vec4 sadd(Vec4 v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		return this;
	}
	
	public Vec4 sadd(float f)
	{
		x += f;
		y += f;
		z += f;
		return this;
	}

	public Vec4 ssub(Vec4 v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return this;
	}

	public Vec4 smul(float f)
	{
		x *= f;
		y *= f;
		z *= f;
		return this;
	}
	
	@Override
	public String toString()
	{
		return String.format("[%.2f %.2f %.2f %.2f]\n", x, y, z, w);
	}
}
