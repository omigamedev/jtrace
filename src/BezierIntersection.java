
class BezierIntersection extends Intersection
{
	float u, v;
	
	public BezierIntersection(float ft, Object3D object, Ray ray, boolean front, float fu, float fv)
	{
		super(ft, object, ray, front);
		u = fu;
		v = fv;
	}

}