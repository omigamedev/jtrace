public class Quad extends Object3D
{
	public Vec4 v0, v1, v2, v3;

	@Override
	public Intersection intersect(Ray ray)
	{
		Vec4 a = v1.sub(v0);
		Vec4 b = v2.sub(v1);
		Vec4 c = v3.sub(v2);
		Vec4 d = v0.sub(v3);
		
		Vec4 n = a.cross(b);
		
		Plane p = new Plane(v0, n, null);
		Intersection is = p.intersect(ray);
		if(is == null) return null;
		p.completeIntersection(ray, is);
		
		if(a.cross(is.p.sub(v0)).dot(n) < 0) return null;
		if(b.cross(is.p.sub(v1)).dot(n) < 0) return null;
		if(c.cross(is.p.sub(v2)).dot(n) < 0) return null;
		if(d.cross(is.p.sub(v3)).dot(n) < 0) return null;
		
		is.obj = this;
		return is;
	}

	@Override
	public void completeIntersection(Ray ray, Intersection is)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void applyMat()
	{
		// TODO Auto-generated method stub

	}

}
