import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

public class AppletMain extends JApplet
{
	@Override
	public void init()
	{
		// Execute a job on the event-dispatching thread; creating this applet's
		// GUI.
		try
		{
			SwingUtilities.invokeAndWait(new Runnable()
			{
				public void run()
				{
					try 
					{
						MainThread main = new MainThread();
						main.start();
					}
					catch (Exception e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
		catch (Exception e)
		{
			System.err.println("createGUI didn't complete successfully");
		}
	}
}

class MainThread extends Thread
{
	@Override
	public void run()
	{
		try
		{
			Main.main(null);
		}
		catch(Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.run();
	}
}