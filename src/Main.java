import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.WritableRaster;
import java.util.concurrent.ArrayBlockingQueue;

import com.sun.swing.internal.plaf.synth.resources.synth;

public class Main
{
	public static int w = 200;
	public static int h = 200;
	public static int downsample = 3;
	public static int samples = 1;
	public static int new_samples = 1;
	public static boolean new_randomize = false;
	public static boolean new_reset = false;
	public static boolean new_view = false;

	public static Viewer view;
	public static Options opt;
	public static WritableRaster raster;
	public static Graphics2D g, g0;
	public static Vec4 light;
	public static int[] color = { 0, 0, 0 };
	public static int[] fpsBuffer = new int[3];
	public static int fpsIndex = 0;
	public static Object3D[] objects = new Object3D[25];
	public static Object3D[] bezlist = new Object3D[1];
	public static Vec4 moving = null;
	public static Matrix4 modelview = new Matrix4();
	public static Matrix4 modelview_inverse = new Matrix4();
	public static Matrix4 modelview_original = new Matrix4();
	public static Ray[][][] rays;
	public static float bezier_z = 1.6f;
	public static Vec2 rot = new Vec2();
	public static Vec2 oldRot = new Vec2();
	public static Bezier3DPatch bp;
	//public static float nearPlane = -2.0f;
	public static Vec4 po = new Vec4(0, 0, -1000); // center of projection
	public static Vec4 new_po = new Vec4(0, 0, -1000); // center of projection
	
	// define some colors
	public static Vec4 cblack = new Vec4();
	public static Vec4 cwhite = new Vec4(1.0f, 1.0f, 1.0f);
	public static Vec4 cred = new Vec4(1.0f, 0.3f, 0.3f);
	public static Vec4 cgreen = new Vec4(0.3f, 1.0f, 0.3f);
	public static Vec4 cblue = new Vec4(0.3f, 0.3f, 1.3f);
	public static Vec4 cgray = new Vec4(0.3f, 0.3f, 0.3f);

	public static int render_startx = 0;
	public static int render_starty = 1;
	public static int render_div = 20;
	public static int render_count = 0;
	public static int render_shift = 1;
	public static Point[][] cp2d;
	
	public static int nthreads = 1;
	public static RenderThread[] threads;
	public static ArrayBlockingQueue<Integer> renderQueue;
	public static int renderCount;
	public static Object renderLock;

	public static void main(String[] args) throws Exception
	{
		if(args.length > 0)
		{
			w = Integer.parseInt(args[0]);
			h = Integer.parseInt(args[1]);
			downsample = Integer.parseInt(args[2]);
		}
		
		int frames = 0;
		view = new Viewer(w, h);
		opt = new Options();
		
		Thread.sleep(100);
		
		raster = view.img.getRaster();
		g0 = (Graphics2D)view.img.getGraphics();
		g0.setBackground(Color.black);
		g = (Graphics2D)view.img2.getGraphics();
		g.setBackground(Color.black);
		g.setColor(Color.red);

		// 2D control points
		cp2d = new Point[4][4];
		for(int i = 0; i < 4; i++)
			for(int j = 0; j < 4; j++) 
				cp2d[i][j] = new Point();
		
		// Init Bezier
		bp = new Bezier3DPatch();
		bp.specular = 1.0f;
		bp.color = cred;
		bezlist[0] = bp;
		objects[0] = bp;

		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				Sphere sp = new Sphere(bp.cp[i][j], 0.1f, cblue);
				//sp.applyMat();
				objects[i * 4 + j + 1] = sp;
			}
		}
		
		// Init threads
		nthreads = Runtime.getRuntime().availableProcessors();
		threads = new RenderThread[nthreads];
		renderQueue = new ArrayBlockingQueue<Integer>(h);
		renderLock = new Object();
		for(int i = 0; i < nthreads; i++)
		{
			threads[i] = new RenderThread(renderQueue);
			threads[i].start();
		}

		randomizeControlPoints();
		
		light = new Vec4(0, 0, 0);

		initRays();

		long time_start = System.currentTimeMillis();
		
		while (true)
		{
			boolean reinitRays = false;
			if(new_samples != samples)
			{
				samples = new_samples;
				reinitRays = true;
			}
			if(new_randomize)
			{
				randomizeControlPoints();
				new_randomize = false;
			}
			if(new_view)
			{
				Main.rot.x = 0;
				Main.rot.y = 0;
				new_view = false;
			}
			if(new_reset)
			{
				resetControlPoints();
				new_reset = false;
			}
			if(new_po.z != po.z)
			{
				po.z = new_po.z;
				reinitRays = true;
			}
			
			if(reinitRays) initRays();
			
			update();

			// Calculate fps
			frames++;
			long time_stop = System.currentTimeMillis();
			if (time_stop - time_start > 1000)
			{
				fpsBuffer[fpsIndex] = frames;
				fpsIndex = (fpsIndex + 1) % fpsBuffer.length;
				view.setTitle("Raytracing " + w + "x" + h + " rays at fps: " + (int)getFPS());
				frames = 0;
				time_start = time_stop;
			}
		}
	}
	
	public static void synchDecrement()
	{
		synchronized (renderLock)
		{
			renderCount--;
			renderLock.notify();
		}
	}
	
	public static void synchReady()
	{
		synchronized (renderLock)
		{
			while(renderCount > 0)
			{
				try
				{
					renderLock.wait();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
					break;
				}
			}
		}
	}
	
	public static void update()
	{
		updateView();
		
		if(Options.chkRaytrace.isSelected() /*&& view.dragStart == null*/)
		{
//			for(int i = 0; i < h; i++) renderQueue.add(i);
//			renderCount = h;
//			for(int i = 0; i < nthreads; i++)
//			{
//				threads[i].unlock();
//			}
//			synchReady();
			raytrace();
		}
		else g0.clearRect(0, 0, view.img.getWidth(), view.img.getHeight());
		
		drawOverlay();
		if(/*view.dragStart != null ||*/ Options.chkWireframe.isSelected() || !Options.chkRaytrace.isSelected()) draw2DBezier();
		
		view.swap();
	}
	
	public static void drawOverlay()
	{
		Plane npl = new Plane(new Vec4(0, 0, 0), new Vec4(0, 0, 1), null);
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				int idx = i * 4 + j;
				Sphere sp = (Sphere)objects[idx + 1];
				Ray ray = new Ray(sp.c, po.sub(sp.c).snormalize());
				float t = npl.intersectUnbound(ray);
				if(t != Float.NaN)
				{
					Vec4 p = ray.o.add(ray.d.mul(t));
					int x = (int)((p.x + 0.5f) * (float)w);
					int y = (int)((-p.y + 0.5f) * (float)h);
					cp2d[i][j].x = x;
					cp2d[i][j].y = y;
				}
			}
		}
		
		g.setBackground(new Color(0, 0, 0, 0));
		g.setColor(new Color(0, 0, 0, 0));
		g.clearRect(0, 0, view.img2.getWidth(), view.img2.getHeight());
		g.setColor(Color.red);
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if(Options.chkControlPolygon.isSelected())
				{
					if(j > 0) g.drawLine(cp2d[i][j].x, cp2d[i][j].y, cp2d[i][j-1].x, cp2d[i][j-1].y);
					if(i > 0) g.drawLine(cp2d[i][j].x, cp2d[i][j].y, cp2d[i-1][j].x, cp2d[i-1][j].y);
				}
			}
		}		
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if(Options.chkControlPoints.isSelected())
				{
					int idx = i * 4 + j;
					Sphere sp = (Sphere)objects[idx + 1];
					if(view.hoverObject == sp) g.setColor(Color.yellow);
					g.fillRect(cp2d[i][j].x-5, cp2d[i][j].y-5, 10, 10);
					g.setColor(Color.red);
				}
			}
		}		
	}

	public static void updateView()
	{
		modelview.sidentity();
		modelview.smul(Matrix4.translation(0, 0, bezier_z));
		modelview.smul(Matrix4.rotationy(-rot.x * 2));
		modelview.smul(Matrix4.rotationx(rot.y * 2));
		// modelview.smul(Matrix4.scale(2, 2, 2));
		modelview_inverse.sidentity();
		modelview_inverse.smul(Matrix4.rotationx(-rot.y * 2));
		modelview_inverse.smul(Matrix4.rotationy(rot.x * 2));
		modelview_inverse.smul(Matrix4.translation(0, 0, -bezier_z));

		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				Sphere sp = (Sphere)objects[i * 4 + j + 1];
				sp.modelview = modelview;
				sp.applyMat();
				bp.cp_original[i][j].x = sp.c.x;
				bp.cp_original[i][j].y = sp.c.y;
				bp.cp_original[i][j].z = sp.c.z;
				bp.applyMat();
			}
		}		
	}

	public static void randomizeControlPoints()
	{
		// Randomize control points
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				int idx = i * 4 + j;
				Sphere sp = (Sphere)objects[idx + 1];
				sp.c_original = new Vec4(0.8f * (float)j - 1.2f, 0.8f * (float)i - 1.2f, 
					(float)Math.random() * 2.0f - 1.0f);
				sp.applyMat();
			}
		}
	}
	
	public static void resetControlPoints()
	{
		// Reset control points
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				int idx = i * 4 + j;
				Sphere sp = (Sphere)objects[idx + 1];
				sp.c_original = new Vec4(0.8f * (float)j - 1.2f, 0.8f * (float)i - 1.2f, 0);
				sp.applyMat();
			}
		}
	}

	private static void drawLine(Vec2 a, Vec2 b)
	{
		g.drawLine((int)a.x, (int)a.y, (int)b.x, (int)b.y);
	}

	public static void draw2DBezier()
	{
		int n = Options.sliderFitting.getValue();
		Point[][] p = new Point[n][n];
		Vec2[][] cp = new Vec2[4][4];
		
		for(int i = 0; i < 4; i++)
			for(int j = 0; j < 4; j++)
				cp[i][j] = new Vec2(cp2d[i][j].x, cp2d[i][j].y);
		Bezier2D b = new Bezier2D(cp);
		
		Vec2[] cptmp = new Vec2[4];
		float ni = 1.0f / n;
		for(int u = 0; u <= n; u++)
		{
			for(int i = 0; i < 4; i++)
			{
				cptmp[i] = b.deCasteljau(cp[i], (float)u * ni, null, null);
			}
			for (int v = 0; v <= n; v++)
			{
				Vec2 pp = b.deCasteljau(cptmp, (float)v * ni, null, null);
				g.fillRect((int)pp.x - 1, (int)pp.y - 1, 3, 3);
			}
		}
	}
	
	public static void raytrace()
	{
		if ((render_count++ % render_div) == 0)
		{
			render_shift = ((render_shift + 1) % render_div);
			render_starty = render_startx + render_shift;
		}
		render_startx = (render_startx + 1) % render_div;
		render_starty = (render_starty + 1) % render_div;
		for (int y = render_starty; y < h; y += render_div)
		{
			for (int x = render_startx; x < w; x += render_div)
			{
				Vec4 frag = new Vec4();
				for (int s = 0; s < samples; s++)
				{
					Ray ray = rays[x][y][s];
					Intersection winner = Intersection.find(ray, bezlist, null);
					if (winner != null)
					{
						winner.complete();
						Vec4 col;
						if(Options.chkNormals.isSelected())
						{
							col = winner.n.add(1.0f).smul(0.5f);
						}
						else
						{
							col = Options.chkTexture.isSelected() ? winner.obj.getColor(winner) : new Vec4(0, 0, 1);
	
							if(Options.chkDiffuse.isSelected()) 
							{
								Vec4 light_vec = light.sub(winner.p);
								Vec4 light_dir = light_vec.normalize();
								// float dist = light_vec.dot(light_vec);
								float att = 1.0f;// Math.min(1.0f / (dist), 1.0f);
								float I = Math.abs(light_dir.dot(winner.n));
								col.smul(I * att);
								
								Vec4 light_ref = light_dir.reflection(winner.n);
								Vec4 eye_dir = ray.o.sub(winner.p).normalize();
								int alpha = Options.sliderPhongAlpha.getValue();
								float phong = (float)Math.pow(Math.max(light_ref.dot(eye_dir), 0), alpha) * winner.obj.specular;
								col.sadd(phong);
							}
							
							float ambient = 0.0f;
	
							// frag.smul(V);
							col.sadd(ambient);
						}
						
						if(winner.p.z > 0) frag.sadd(col.smul(1.0f / (float)samples));
					}
				}
				// set(x, h - y - 1, winner.n.add(1.0f).mul(0.5f));
//				frag;
				frag.sclamp(0, 1);
				set(x, h - y - 1, frag);
			}
		}
	}
	
	public static void raytrace_threaded(int y)
	{
		for (int x = 0; x < w; x++)
		{
			//if(view.dragStart != null) return;
			Vec4 frag = new Vec4();
			for (int s = 0; s < samples; s++)
			{
				Ray ray = rays[x][y][s];
				Intersection winner = Intersection.find(ray, bezlist, null);
				if (winner != null)
				{
					winner.complete();
					Vec4 col;
					if(Options.chkNormals.isSelected())
					{
						col = winner.n.add(1.0f).smul(0.5f);
					}
					else
					{
						col = Options.chkTexture.isSelected() ? winner.obj.getColor(winner) : new Vec4(0, 0, 1);

						if(Options.chkDiffuse.isSelected()) 
						{
							Vec4 light_vec = light.sub(winner.p);
							Vec4 light_dir = light_vec.normalize();
							// float dist = light_vec.dot(light_vec);
							float att = 1.0f;// Math.min(1.0f / (dist), 1.0f);
							float I = Math.abs(light_dir.dot(winner.n));
							col.smul(I * att);
							
							Vec4 light_ref = light_dir.reflection(winner.n);
							Vec4 eye_dir = ray.o.sub(winner.p).normalize();
							int alpha = Options.sliderPhongAlpha.getValue();
							float phong = (float)Math.pow(Math.max(light_ref.dot(eye_dir), 0), alpha) * winner.obj.specular;
							col.sadd(phong);
						}
						
						float ambient = 0.0f;

						// frag.smul(V);
						col.sadd(ambient);
					}
					
					if(winner.p.z > 0) frag.sadd(col.smul(1.0f / (float)samples));
				}
			}
			// set(x, h - y - 1, winner.n.add(1.0f).mul(0.5f));
//				frag;
			frag.sclamp(0, 1);
			set(x, h - y - 1, frag);
		}
	}

	public static Ray createRay(float u, float v)
	{
		Vec4 o = new Vec4(u, v, 0);
		Vec4 d = o.sub(po).snormalize();
		return new Ray(o, d);
	}

	public static void initRays()
	{
		float onew = 1.0f / (float)w;
		float oneh = 1.0f / (float)h;
		if(rays == null || rays[0][0].length != samples) rays = new Ray[h][w][samples];
		boolean ss = Options.chkSupersampling.isSelected(); 
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				//for (int s = 0; s < samples; s++)
				{
					float dx = onew * 0.5f;
					float dy = oneh * 0.5f;
					//float center = s == 0 ? 0 : 1;
					float u = (float)x * onew - 0.5f;
					float v = (float)y * oneh - 0.5f;
					rays[x][y][0] = createRay(u, v);
					if(ss)
					{
						rays[x][y][1] = createRay(u+dx, v+dy);
						rays[x][y][2] = createRay(u+dx, v-dy);
						rays[x][y][3] = createRay(u-dx, v+dy);
						rays[x][y][4] = createRay(u-dx, v-dy);
					}
				}
			}
		}
	}

	public static float getFPS()
	{

		float sum = 0.0f;
		int n = 0;
		for (float f : fpsBuffer)
		{
			if (f > 0.0f)
			{
				sum += f;
				n++;
			}
		}
		return sum / (float)n;
	}

	public synchronized static void set(int x, int y, Vec4 c)
	{
		color[0] = (int)(c.x * 255.0f);
		color[1] = (int)(c.y * 255.0f);
		color[2] = (int)(c.z * 255.0f);
		raster.setPixel(x, y, color);
	}

	public static int rgb(int r, int g, int b)
	{
		return ((r & 0x0ff) << 16) | ((g & 0x0ff) << 8) | (b & 0x0ff);
	}
}

class RenderThread extends Thread
{
	ArrayBlockingQueue<Integer> q;
	public Object lock = new Object();
	
	
	public RenderThread(ArrayBlockingQueue<Integer> queue)
	{
		q = queue;
	}
	
	public void unlock()
	{
		synchronized (lock)
		{
			lock.notify();
		}
	}
	
	@Override
	public void run()
	{
		while(true)
		{
			try
			{
				if(q.isEmpty())
				{
					synchronized (lock)
					{
						lock.wait();
					}
				}
				Main.raytrace_threaded(q.take());
				Main.synchDecrement();
			} catch (InterruptedException e)
			{
				e.printStackTrace();
				break;
			}
		}
	}
}
