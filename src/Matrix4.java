public class Matrix4
{
	public float m00, m01, m02, m03;
	public float m10, m11, m12, m13;
	public float m20, m21, m22, m23;
	public float m30, m31, m32, m33;

	public Matrix4()
	{
		sidentity();
	}
	
	public Matrix4 copy(Matrix4 m)
	{
		m00 = m.m00; 
		m01 = m.m01; 
		m02 = m.m02; 
		m03 = m.m03; 
		m10 = m.m10; 
		m11 = m.m11; 
		m12 = m.m12; 
		m13 = m.m13; 
		m20 = m.m20; 
		m21 = m.m21; 
		m22 = m.m22; 
		m23 = m.m23; 
		m30 = m.m30; 
		m31 = m.m31; 
		m32 = m.m32; 
		m33 = m.m33; 
		return this;
	}
	
	public Matrix4 sidentity()
	{
		m00 = m01 = m02 = m03 = 0.0f;
		m10 = m11 = m12 = m13 = 0.0f;
		m20 = m21 = m22 = m23 = 0.0f;
		m30 = m31 = m32 = m33 = 0.0f;
		m00 = m11 = m22 = m33 = 1.0f;
		return this;
	}
	
	public static Matrix4 scale(float x, float y, float z)
	{
		Matrix4 m = new Matrix4();
		m.m00 = x;
		m.m11 = y;
		m.m22 = z;
		return m;		
	}
	
	public static Matrix4 rotationx(float rad)
	{
		Matrix4 ret = new Matrix4();
		float cos = (float)Math.cos(rad);
		float sin = (float)Math.sin(rad);
		ret.m11 = cos;
		ret.m12 = -sin;
		ret.m21 = sin;
		ret.m22 = cos;
		return ret;
	}

	public static Matrix4 rotationy(float rad)
	{
		Matrix4 ret = new Matrix4();
		float cos = (float)Math.cos(rad);
		float sin = (float)Math.sin(rad);
		ret.m00 = cos;
		ret.m02 = sin;
		ret.m20 = -sin;
		ret.m22 = cos;
		return ret;
	}

	public static Matrix4 rotationz(float rad)
	{
		Matrix4 ret = new Matrix4();
		float cos = (float)Math.cos(rad);
		float sin = (float)Math.sin(rad);
		ret.m00 = cos;
		ret.m01 = sin;
		ret.m10 = -sin;
		ret.m11 = cos;
		return ret;
	}
	
	public static Matrix4 translation(float x, float y, float z)
	{
		Matrix4 m = new Matrix4();
		m.m03 = x;
		m.m13 = y;
		m.m23 = z;
		return m;
	}

	public static Matrix4 identity()
	{
		return new Matrix4();
	}

	public Vec4 smul(Vec4 v)
	{
		float x = v.x, y = v.y, z = v.z, w = v.w;
		v.x = m00 * x + m01 * y + m02 * z + m03 * w;
		v.y = m10 * x + m11 * y + m12 * z + m13 * w;
		v.z = m20 * x + m21 * y + m22 * z + m23 * w;
		v.w = m30 * x + m31 * y + m32 * z + m33 * w;
		return v;
	}

	public Vec4 mul(Vec4 v)
	{
		Vec4 ret = new Vec4();
		ret.x = m00 * v.x + m01 * v.y + m02 * v.z + m03 * v.w;
		ret.y = m10 * v.x + m11 * v.y + m12 * v.z + m13 * v.w;
		ret.z = m20 * v.x + m21 * v.y + m22 * v.z + m23 * v.w;
		ret.w = m30 * v.x + m31 * v.y + m32 * v.z + m33 * v.w;
		return ret;
	}

	public Matrix4 smul(Matrix4 m)
	{
		float m00 = this.m00 * m.m00 + this.m01 * m.m10 + this.m02 * m.m20 + this.m03 * m.m30;
		float m01 = this.m00 * m.m01 + this.m01 * m.m11 + this.m02 * m.m21 + this.m03 * m.m31;
		float m02 = this.m00 * m.m02 + this.m01 * m.m12 + this.m02 * m.m22 + this.m03 * m.m32;
		float m03 = this.m00 * m.m03 + this.m01 * m.m13 + this.m02 * m.m23 + this.m03 * m.m33;
		float m10 = this.m10 * m.m00 + this.m11 * m.m10 + this.m12 * m.m20 + this.m13 * m.m30;
		float m11 = this.m10 * m.m01 + this.m11 * m.m11 + this.m12 * m.m21 + this.m13 * m.m31;
		float m12 = this.m10 * m.m02 + this.m11 * m.m12 + this.m12 * m.m22 + this.m13 * m.m32;
		float m13 = this.m10 * m.m03 + this.m11 * m.m13 + this.m12 * m.m23 + this.m13 * m.m33;
		float m20 = this.m20 * m.m00 + this.m21 * m.m10 + this.m22 * m.m20 + this.m23 * m.m30;
		float m21 = this.m20 * m.m01 + this.m21 * m.m11 + this.m22 * m.m21 + this.m23 * m.m31;
		float m22 = this.m20 * m.m02 + this.m21 * m.m12 + this.m22 * m.m22 + this.m23 * m.m32;
		float m23 = this.m20 * m.m03 + this.m21 * m.m13 + this.m22 * m.m23 + this.m23 * m.m33;
		float m30 = this.m30 * m.m00 + this.m31 * m.m10 + this.m32 * m.m20 + this.m33 * m.m30;
		float m31 = this.m30 * m.m01 + this.m31 * m.m11 + this.m32 * m.m21 + this.m33 * m.m31;
		float m32 = this.m30 * m.m02 + this.m31 * m.m12 + this.m32 * m.m22 + this.m33 * m.m32;
		float m33 = this.m30 * m.m03 + this.m31 * m.m13 + this.m32 * m.m23 + this.m33 * m.m33;
		this.m00 = m00;
		this.m01 = m01;
		this.m02 = m02;
		this.m03 = m03;
		this.m10 = m10;
		this.m11 = m11;
		this.m12 = m12;
		this.m13 = m13;
		this.m20 = m20;
		this.m21 = m21;
		this.m22 = m22;
		this.m23 = m23;
		this.m30 = m30;
		this.m31 = m31;
		this.m32 = m32;
		this.m33 = m33;		
		return this;
	}
	
	public Matrix4 smul(float f)
	{
		this.m00 *= f;
		this.m01 *= f;
		this.m02 *= f;
		this.m03 *= f;
		this.m10 *= f;
		this.m11 *= f;
		this.m12 *= f;
		this.m13 *= f;
		this.m20 *= f;
		this.m21 *= f;
		this.m22 *= f;
		this.m23 *= f;
		this.m30 *= f;
		this.m31 *= f;
		this.m32 *= f;
		this.m33 *= f;		
		return this;
	}
	
	public Matrix4 stranspose()
	{
		float m00 = this.m00;
		float m01 = this.m00;
		float m02 = this.m00;
		float m03 = this.m00;
		float m10 = this.m10;
		float m11 = this.m10;
		float m12 = this.m10;
		float m13 = this.m10;
		float m20 = this.m20;
		float m21 = this.m20;
		float m22 = this.m20;
		float m23 = this.m20;
		float m30 = this.m30;
		float m31 = this.m30;
		float m32 = this.m30;
		float m33 = this.m30;
		this.m00 = m00;
		this.m01 = m10;
		this.m02 = m20;
		this.m03 = m30;
		this.m10 = m01;
		this.m11 = m11;
		this.m12 = m21;
		this.m13 = m31;
		this.m20 = m02;
		this.m21 = m12;
		this.m22 = m22;
		this.m23 = m32;
		this.m30 = m03;
		this.m31 = m13;
		this.m32 = m23;
		this.m33 = m33;		
		return this;
	}

	public Matrix4 transpose()
	{
		Matrix4 ret = new Matrix4();
		ret.copy(this);
		ret.stranspose();
		return ret;
	}

	public Matrix4 mul(Matrix4 m)
	{
		Matrix4 ret = new Matrix4();
		ret.copy(this);
		ret.smul(m);
		return ret;
	}

	public Matrix4 mul(float f)
	{
		Matrix4 ret = new Matrix4();
		ret.copy(this);
		ret.smul(f);
		return ret;
	}

	@Override
	public String toString()
	{
		return String.format("%.2f %.2f %.2f %.2f\n" + "%.2f %.2f %.2f %.2f\n" + "%.2f %.2f %.2f %.2f\n"
				+ "%.2f %.2f %.2f %.2f\n", m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32,
				m33);
	}
}
