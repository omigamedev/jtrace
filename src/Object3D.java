public abstract class Object3D
{
	public float specular;
	public Vec4 color;
	public Matrix4 model;
	public Matrix4 modelview;

	public Object3D()
	{
		specular = 0.0f;
		model = new Matrix4();
		modelview = new Matrix4();
	}

	abstract public Intersection intersect(Ray ray);

	abstract public void completeIntersection(Ray ray, Intersection is);

	abstract public void applyMat();
	
	public Vec4 getColor(Intersection is)
	{
		return color.clone();
	}
	
	public void applyView(Matrix4 view)
	{
		modelview.copy(model);
		modelview.smul(view);
	}
}
